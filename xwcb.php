<?php

  if (isset($_GET['date'])) {
    $date = strtotime($_GET['date']);
  }
  if(!isset($date) || !$date) {
    $date = strtotime('now');
  }

  function getcontent($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11');
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    do {
      $content = curl_exec($ch);
    } while ($content == '');
    curl_close($ch);
    return $content;
  }

  function match($content, $match) {
    preg_match_all($match, $content, $matchcontent, PREG_PATTERN_ORDER);
    return $matchcontent;
  }

  $xwcb = getcontent('http://newspaper.jfdaily.com/xwcb/html/'.date('Y-m/d', $date).'/node_2.htm');
  $xwcb = match($xwcb, '/xwcb\/files\/[\d]{8}\/[\d]+.PDF/');
  $xwcb = $xwcb[0];

  $count = count($xwcb);
  if (!$count) {
    exit('<h1>失败！</h1>');
  }
  for($i=0; $i<$count; $i++) {
    $content = getcontent('http://newspaper.jfdaily.com/'.$xwcb[$i]);
    file_put_contents('temp/txwcb_'.date('Ymd_').sprintf('%03d', $i).'.pdf', $content);
  }
  exec('pdftk temp/txwcb_'.date('Ymd').'_*.pdf cat output XWCB_'.date('Ymd', $date).'.pdf');
  exec('rm -f temp/txwcb_'.date('Ymd').'_*.pdf');
  echo '<h1>成功保存至当前目录！</h1>';
?>