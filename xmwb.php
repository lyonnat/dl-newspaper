<?php

  if (isset($_GET['date'])) {
    $date = strtotime($_GET['date']);
  }
  if(!isset($date) || !$date) {
    $date = strtotime('now');
  }

  function getcontent($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11');
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    do {
      $content = curl_exec($ch);
    } while ($content == '');
    curl_close($ch);
    return $content;
  }

  function match($content, $match) {
    preg_match_all($match, $content, $matchcontent, PREG_PATTERN_ORDER);
    return $matchcontent;
  }

  $xmwb = getcontent('http://xmwb.xinmin.cn/html/'.date('Y-m/d', $date).'/node_1.htm');
  $xmwb = match($xmwb, '/resfile\/[\d]{4}-[\d]{2}-[\d]{2}\/([A-Za-z]{1}[\d]{2})\/[A-Za-z0-9]+\.pdf/');
  $xmwb[0] = array_unique($xmwb[0]);
  $xmwb[1] = array_unique($xmwb[1]);

  $count = count($xmwb[0]);
  if (!$count) {
    exit('<h1>失败！</h1>');
  }
  for($i=0; $i<$count; $i++) {
    $content = getcontent('http://xmwb.xinmin.cn/'.$xmwb[0][$i]);
    file_put_contents('temp/txmwb_'.date('Ymd_').$xmwb[1][$i].'.pdf', $content);
  }
  exec('pdftk temp/txmwb_'.date('Ymd').'_*.pdf cat output XMWB_'.date('Ymd', $date).'.pdf');
  exec('rm -f temp/txmwb_'.date('Ymd').'_*.pdf');
  echo '<h1>成功保存至当前目录！</h1>';
?>